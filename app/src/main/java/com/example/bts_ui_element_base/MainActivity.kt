package com.example.bts_ui_element_base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*

class MainActivity : AppCompatActivity() {

    private var TAG:String = "MainActivity"
    private lateinit var button:Button
    private lateinit var switch: Switch
    private lateinit var checkBox: CheckBox
    private lateinit var toggleButton: ToggleButton
    private lateinit var progressBar: ProgressBar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById(R.id.button)
        button.setOnClickListener{ view ->
            Toast.makeText(this, "You have pressed the button!!!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "You have pressed the button, here is the log!!")
        }

        switch = findViewById(R.id.switch1)
        switch.setOnCheckedChangeListener { buttonview, isChecked ->
            Toast.makeText(this, isChecked.toString(), Toast.LENGTH_SHORT).show()
            Log.d(TAG, "The switch has been flipped.")
        }

        checkBox = findViewById(R.id.checkBox)
        checkBox.setOnCheckedChangeListener { buttonview, isChecked ->
            Toast.makeText(this, "The checkbox has been checked.", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "The log for the checkbox.")
        }

        toggleButton = findViewById(R.id.toggleButton)
        toggleButton.setOnCheckedChangeListener { buttonview, isChecked ->
            Toast.makeText(this, "The other button has been pressed!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "The button has been pressed.")
        }

        progressBar = findViewById(R.id.progressBar2)


    }
}